
/**
 * Database Profile model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 17-June-2020
 * Last Updated: 17-June-2020
 */

var moment = require('moment');
 
'use strict';
module.exports = function(sequelize, Sequelize) {
    
  var Category = sequelize.define('Category', {
  
    categoryName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    
  });
  
  
  
  Category.associate = function(models) {
    models.Category.hasMany(models.Document);

  }
  return Category;
};