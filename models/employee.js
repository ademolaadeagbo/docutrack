
/**
 * Database Employee model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 19-June-2020
 * Last Updated: 20-June-2020
 */


var moment = require('moment');

'use strict';
 
module.exports = function(sequelize, Sequelize) {

    var Employee = sequelize.define('Employee', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },

        firstName: {
            type: Sequelize.STRING,
            allowNull: false,
        },

        lastName: {
            type: Sequelize.STRING,
            allowNull: false,
        },

        userName: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
        },
        
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        
        phone: {
            type: Sequelize.INTEGER,
            validate: {
                isNumeric: true,  
            }
        },
        
        address: {
            type: Sequelize.TEXT,
        },

        DepartmentId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        
        RoleId: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
       

    });
    
    
    
    Employee.associate = function (models) {
        
    models.Employee.belongsTo(models.Role, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    
    
     models.Employee.belongsTo(models.Department, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    
     models.Employee.belongsToMany(models.Permission,{ 
       as: 'permissions', 
       through: 'EmployeePermissions',
       foreignKey: 'employeeId'
     });
  
     models.Employee.hasMany(models.Document);
  
    };
    
     return Employee;

};

 
    