/**
 * Database Application model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 23-June-2020
 * Last Updated: 23-June-2020
 */


var moment = require('moment');
 
'use strict';
module.exports = function(sequelize, Sequelize) {
    
  var Application = sequelize.define('Application', {
  
    applicationName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    
  });
  
  
  
  Application.associate = function(models) {
   models.Application.hasMany(models.Document);
    
  };
  return Application;
};
