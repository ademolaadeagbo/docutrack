/**
 * Database Type model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 23-June-2020
 * Last Updated: 23-June-2020
 */


var moment = require('moment');
 
'use strict';
module.exports = function(sequelize, Sequelize) {
    
  var Type = sequelize.define('Type', {
  
    typeName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    
  });
  
  
  
  Type.associate = function(models) {
   models.Type.hasMany(models.Document);
    
  };
  return Type;
};