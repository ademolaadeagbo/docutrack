
/**
 * Database Profile model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 23-June-2020
 * Last Updated: 23-June-2020
 */

var moment = require('moment');

'use strict';
module.exports = function(sequelize, Sequelize){
  var Document = sequelize.define('Document', {
      
    subject:  {
          type: Sequelize.STRING,
          allowNull: false
        },
        
    description:{
          type: Sequelize.TEXT,
          allowNull: false
        },
        
    status: {
            type: Sequelize.ENUM('draft', 'published'),
            defaultValue: 'draft'
        },
        
        
    ApplicationId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        
    TypeId:  {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
    CategoryId:  {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        
    EmployeeId:  {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        
     RoleId:  {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        
     DepartmentId:  {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
  });
  
  Document.associate = function (models) {
    models.Document.belongsTo(models.Category,{ 
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    
    
    models.Document.belongsTo(models.Department, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    
    
    models.Document.belongsTo(models.Role, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    
    
    
    models.Document.belongsTo(models.Application, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    
    
    models.Document.belongsTo(models.Type, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    
    

    models.Document.belongsTo(models.Employee, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });

    models.Document.hasMany(models.Comment);
  
  };

  
  return Document;
};
