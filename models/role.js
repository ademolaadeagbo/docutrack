
/**
 * Database Role model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 23-June-2020
 * Last Updated: 23-June-2020
 */

var moment = require('moment');
 
'use strict';
module.exports = function(sequelize, Sequelize) {
    
  var Role = sequelize.define('Role', {
  
    roleName: {
      type: Sequelize.STRING,
      allowNull: false,
     
    },
    
  });
  
  
  
  Role.associate = function(models) {
    models.Role.hasMany(models.Employee);
    models.Role.hasMany(models.Document);
  };


  return Role;
};
