/**
 * Database Permission model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 23-June-2020
 * Last Updated: 23-June-2020
 */

'use strict';
module.exports = function(sequelize, Sequelize){
  var Permission = sequelize.define('Permission', {
     permissionName: {
        type: Sequelize.STRING,
        allowNull: false,
        }
  });
 
  // create association between permission and employee
  // a permission can have many employees
   Permission.associate = function(models) {
     models.Permission.belongsToMany(models.Employee,{ 
       as: 'employees', 
       through: 'EmployeePermissions',
       foreignKey: 'permissionId'
     });
   };
  
  return Permission;
};
 