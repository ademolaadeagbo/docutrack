
/**
 * Database Profile model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 17-June-2020
 * Last Updated: 17-June-2020
 */

var moment = require('moment');



'use strict';
module.exports = function(sequelize, Sequelize) {
  var Comment = sequelize.define('Comment', {
    
   commentBody: Sequelize.TEXT
   
  });
  
  Comment.associate = function (models) {
    models.Comment.belongsTo(models.Document, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
  };
  
  return Comment;
};


// Make sure you complete other models fields