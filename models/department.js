/**
 * Database Department model.
 * Author: Ademola Adeagbo.
 * Version: 1.0.0
 * Release Date: 17-June-2020
 * Last Updated: 17-June-2020
 */

var moment = require('moment');
 
 'use strict';
module.exports = function(sequelize, Sequelize) {
    
  var Department = sequelize.define('Department', {
  
    departmentName: { 
      type: Sequelize.STRING,
      allowNull: false
    },
    
  });
  
  
  
  Department.associate = function(models) {
    models.Department.hasMany(models.Employee);
    models.Department.hasMany(models.Document);
  };


  return Department;
};
