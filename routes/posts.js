var express = require('express');
var router = express.Router();


let post_controller = require("../controllers/postController");


// GET request for creating post.
router.get('/create', post_controller.post_create_get);

// POST request for creating post.
router.post('/create', post_controller.post_create_post);

// GET request to delete post.
router.get('/:post_id/delete', post_controller.post_delete_get);

// POST request to delete post
router.post('/:post_id/delete', post_controller.post_delete_post);

// GET request to update post.
router.get('/:post_id/update', post_controller.post_update_get);

// POST request to update post.
router.post('/:post_id/update', post_controller.post_update_post);

//READ ALL POSTS Details
//router.get('/:post_id', post_controller.posts_by_user_get);

//READ ALL POSTS BY A USER
router.get('/:user_id', post_controller.posts_by_user_get);

 //READ ALL POSTS BY A USER IN A DEPARTMENT
//router.get('/:user_id/:department_id', post_controller.posts_by_user_department_posts);



// GET request for list of all posts.
router.get('/posts', post_controller.post_list);



/* GET posts listing. */
router.get('/', post_controller.post_list);

module.exports = router;
