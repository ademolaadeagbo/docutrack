var express = require('express');
var router = express.Router();



// Require our controllers.
var employee_controller = require('../controllers/api/employeeController');
var department_controller = require('../controllers/api/departmentController');
var role_controller = require('../controllers/api/roleController');
var document_controller = require('../controllers/api/documentController');
var application_controller = require('../controllers/api/applicationController');
var category_controller = require('../controllers/api/categoryController');
var type_controller = require('../controllers/api/typeController');
var permission_controller = require('../controllers/api/permissionController');
var comment_controller = require('../controllers/api/commentController');






// EMPLOYEE ROUTES



// POST REQUEST FOR CREATING EMPLOYEE
router.post('/employee/create', employee_controller.employee_create_post);


// POST REQUEST FOR CREATING EMPLOYEE
router.post('/employee/:employee_id/delete', employee_controller.employee_delete_post);


// POST REQUEST FOR UPDATING EMPLOYEE
router.post('/employee/:employee_id/update', employee_controller.employee_update_post);

// GET REQUEST FOR ONE EMPLOYEE
router.get('/employee/:employee_id', employee_controller.employee_detail);

//GET REQUEST FOR ALL EMPLOYEE
router.get('/employees', employee_controller.employee_list);




// DEPARTMENT ROUTES



// POST REQUEST FOR CREATING DEPARTMENT
router.post('/department/create', department_controller.department_create_post);

// POST REQUEST FOR DELETING DEPARTMENT
router.post('/department/:department_id/delete', department_controller.department_delete_post);

// POST REQUEST FOR UPDATIGN DEPARTMENT
router.post('/department/:department_id/update', department_controller.department_update_post);

// GET REQUEST FOR A DEPARTMENT
router.get('/department/:department_id', department_controller.department_detail);

// GET REQUEST FOR ALL DEPARTMENT
router.get('/departments', department_controller.department_list);




// ROLE ROUTES


// POST REQUEST FOR CREATING ROLE
router.post('/role/create', role_controller.role_create_post);

// POST REQUEST FOR DELETING ROLE
router.post('/role/:role_id/delete', role_controller.role_delete_post);

// POST REQUEST FOR UPDATING ROLE
router.post('/role/:role_id/update', role_controller.role_update_post);

// GET REQUEST FOR A ROLE
router.get('/role/:role_id', role_controller.role_detail);

// GET REQUEST FOR ALL ROLE
router.get('/roles', role_controller.role_list);




// CATEGORY ROUTES



// POST REQUEST FOR CREATING CATEGORY
router.post('/category/create', category_controller.category_create_post);


// POST REQUEST FOR DELETING CATEGORY
router.post('/category/:category_id/delete', category_controller.category_delete_post);

// POST REQUEST FOR DELETING CATEGORY
router.post('/category/:category_id/update', category_controller.category_update_post);

// GET REQUEST FOR A CATEGORY
router.get('/category/:category_id', category_controller.category_detail);

// GET REQUEST FOR ALL CATEGORY
router.get('/categories', category_controller.category_list);




// APPLICATION ROUTES


// POST REQUEST FOR CREATING APPLICATION
router.post('/application/create', application_controller.application_create_post);


// GET REQUEST FOR DELETING APPLICATION
router.post('/application/:application_id/delete', application_controller.application_delete_post);

// POST REQUEST FOR UPDATING APPLICATION
router.post('/application/:application_id/update', application_controller.application_update_post);

// GET REQUEST FOR A APPLICATION
router.get('/application/:application_id', application_controller.application_detail);

// GET REQUEST FOR ALL APPLICATION
router.get('/applications', application_controller.application_list);



// DOCUMENT ROUTES


// POST REQUEST FOR CREATING DOCUMENT// POST request for Creating a DOCUMENT
router.post('/document/create', document_controller.document_create_post);


// GET REQUEST FOR DELETING DOCUMENT
router.post('/document/:document_id/delete', document_controller.document_delete_post);


// POST REQUEST FOR UPDATING DOCUMENT
router.post('/document/:document_id/update', document_controller.document_update_post);

// GET REQUEST FOR A DOCUMENT
router.get('/document/:document_id', document_controller.document_detail);

// GET REQUEST FOR ALL DOCUMENT
router.get('/documents', document_controller.document_list);



// COMMENT ROUTES


// POST REQUEST FOR CREATING COMMENT
router.post('/comment/create', comment_controller.comment_create_post);


// GET REQUEST FOR DELETING COMMENT
router.post('/comment/:comment_id/delete', comment_controller.comment_delete_post);


// POST REQUEST FOR UPDATING COMMENT
router.post('/comment/:comment_id/update', comment_controller.comment_update_post);

// GET REQUEST FOR A COMMENT
router.get('/comment/:comment_id', comment_controller.comment_detail);

// GET REQUEST FOR ALL COMMENT
router.get('/comments', comment_controller.comment_list);



// TYPE ROUTES


// POST REQUEST FOR CREATING TYPE
router.post('/type/create', type_controller.type_create_post);


// GET REQUEST FOR DELETING TYPE
router.post('/type/:type_id/delete', type_controller.type_delete_post);

// POST REQUEST FOR UPDATING TYPE
router.post('/type/:type_id/update', type_controller.type_update_post);

// GET REQUEST FOR A TYPE
router.get('/type/:type_id', type_controller.type_detail);

// GET REQUEST FOR ALL TYPE
router.get('/types', type_controller.type_list);



// PERMISSION ROUTES


// POST REQUEST FOR CREATING PERMISSION
router.post('/permission/create', permission_controller.permission_create_post);


// GET REQUEST FOR DELETING PERMISSION
router.post('/permission/:permission_id/delete', permission_controller.permission_delete_post);

// POST REQUEST FOR UPDATING PERMISSION
router.post('/permission/:permission_id/update', permission_controller.permission_update_post);

// GET REQUEST FOR A PERMISSION
router.get('/permission/:permission_id', permission_controller.permission_detail);

// GET REQUEST FOR ALL PERMISSION
router.get('/permissions', permission_controller.permission_list);


//router.get('/', employee_controller.index);







// export all the routers created
module.exports = router;
