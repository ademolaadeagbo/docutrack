var Comment = require('../models/comment');
var models = require('../models');

 
// Display comment create form on GET.
exports.comment_create_get = function(req, res, next) {
        // create comment GET controller logic here 
        res.render('forms/comment_form', { title: 'Create Comment', layout: 'layouts/detail'});
        console.log("comment form renders successfully");
};

// Handle comment create on POST.
exports.comment_create_post = function(req, res, next) {
     // create comment POST controller logic here
     // If a comment gets created successfully, we just redirect to comments list
     // no need to render a page
      models.Comment.create({
            commentBody: req.body.comment,
        }).then(function() {
            console.log("comment created successfully");
           // check if there was an error during comment creation
            res.redirect('/main/comments');
      });
};

// Display comment delete form on GET.
exports.comment_delete_get = function(req, res, next) {
        // GET logic to delete a comment here
        
        // renders comment delete page
        models.Comment.destroy({
            // find the comment_id to delete from database
            where: {
              id: req.params.comment_id
            }
          }).then(function() {
           // If a comment gets deleted successfully, we just redirect comments list
           // no need to render a page
            res.redirect('/main/comments');
            console.log("comment deleted successfully");
          });
};

// Handle comment delete on POST.
exports.comment_delete_post = function(req, res, next) {
        // POST logic to delete a comment here
        // If an comment gets deleted successfully, we just redirect to comments list
        // no need to render a page
        models.Comment.destroy({
            // find the comment_id to delete from database
            where: {
              id: req.params.comment_id
            }
          }).then(function() {
           // If an comment gets deleted successfully, we just redirect to comments list
           // no need to render a page
            res.redirect('/main/comments');
            console.log("comment deleted successfully");
          });
};

// Display comment update form on GET.
exports.comment_update_get = function(req, res, next) {
        // GET logic to update a comment here
        
        console.log("ID is " + req.params.comment_id);
        models.Comment.findById(
                req.params.comment_id
        ).then(function(comment) {
               // renders a comment form
               res.render('forms/comment_form', { title: 'Update Comment', comment: comment, layout: 'layouts/detail'});
               console.log("comment update get successful");
          });
};

// Handle post update on POST.
exports.comment_update_post = function(req, res, next) {
        // POST logic to update an comment here
        // If an comment gets updated successfully, we just redirect to comments list
        // no need to render a page
        models.Comment.update(
        // Values to update
            {
                commentBody: req.body.comment
            },
          { // Clause
                where: 
                {
                    id: req.params.comment_id
                }
            }
        //   returning: true, where: {id: req.params.comment_id} 
         ).then(function() { 
                // If a comment updated successfully, we just redirect to comments list
                // no need to render a page
                res.redirect("/main/comments");  
                console.log("comment updated successfully");
          });
};

// Display list of all comments.
exports.comment_list = function(req, res, next) {
        // GET controller logic to list all comments
         models.Comment.findAll(
        ).then(function(comments) {
       // renders all comments list
        console.log("rendering comment list");
        res.render('pages/comment_list', { title: 'Comment List', comments: comments, layout: 'layouts/list'} );
        console.log("comments list renders successfully");
        });
};

// Display detail page for a specific comment.
exports.comment_detail = async function(req, res, next) {
        // GET controller logic to display just one comment
        
        // renders an individual comment details page
         const documents = await models.Document.findAll();
         models.Comment.findById(
                req.params.comment_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(comment) {
        // renders an inividual comment details page
        res.render('pages/comment_detail', { title: 'Comment Details', comment: comment, documents: documents, layout: 'layouts/detail'} );
        console.log("comment details renders successfully");
        });
};