var Employee = require('../models/employee');
var models = require('../models');
var async = require('async');

// Display employee create form on GET.
exports.employee_create_get = async function(req, res, next) {
        // create employee GET controller logic here 
        
        const roles = await models.Role.findAll();
        const departments = await models.Department.findAll();
      
        res.render('forms/employee_form', { title: 'Create Employee', roles: roles, departments: departments, layout: 'layouts/detail'});
        console.log("employee form renders successfully");
};



// Handle employee create on POST.
exports.employee_create_post = function(req, res, next) {
     // create employee POST controller logic here
     // If an employee gets created successfully, we just redirect to employees list
     // no need to render a page
    models.Employee.create({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            userName: req.body.userName,
            email: req.body.email,
            phone: req.body.phone,
            address: req.body.address,
            DepartmentId: req.body.department,
            RoleId: req.body.role,
        });
        
        console.log("Employee created successfully");
        res.redirect('/main/employees');
};


exports.employee_delete_get = function(req, res, next) {
        // GET logic to delete a employee here

         models.Employee.destroy({
            // find the employee_id to delete from database
            where: {
              id: req.params.employee_id
            }
          }).then(function() {
          
        res.redirect('/main/employees');
        console.log("employee deleted successfully");
        });

};

// Handle employee delete on POST.
exports.employee_delete_post = function(req, res, next) {
        // POST logic to delete a employee here
        

         models.Employee.destroy({
            // find the employee_id to delete from database
            where: {
              id: req.params.employee_id
            }
          }).then(function() {
         
         res.redirect('/main/employees');
        console.log("employee deleted successfully");
          });

};

// Display employee update form on GET.
exports.employee_update_get = async function(req, res, next) {
        // GET logic to update a employee here

        const roles = await models.Role.findAll();
        const departments = await models.Department.findAll();


        console.log("ID is " + req.params.employee_id);
        models.Employee.findById(
                req.params.employee_id
        ).then(function(employee) {
              
               
        res.render('forms/employee_form', { title: 'Update Employee', employee: employee, roles: roles, departments: departments,  layout: 'layouts/detail' });
        console.log("employee update get successful");
          });

};

// Handle employee update on POST.
exports.employee_update_post =  function(req, res, next) {
        // POST logic to update a employee here

        



         models.Employee.update(
        // Values to update
            {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                userName: req.body.userName,
                email: req.body.email,
                phone: req.body.phone,
                address: req.body.address,
                DepartmentId: req.body.department,
                RoleId: req.body.role,
            },
            { // Clause
                where: 
                {
                    id: req.params.employee_id
                }
            }
       
         ).then(function() { 
              
          
        res.redirect("/main/employees");
        console.log("employee updated successfully");
         });

};







exports.employee_list = function(req, res, next) {
        // GET controller logic to list all employees
        
        
        
        models.Employee.findAll({
         include: [{
                        
                  model: models.Role,
                  attributes: ['id', 'roleName']
             
                },
                {
                        
                  model: models.Department,
                  attributes: ['id', 'departmentName']
             
                }]
        
        }).then(function(employees) {
       // renders all employees list
        console.log("rendering employee list");
        res.render('pages/employee_list', { title: 'Employee List', employees: employees, layout: 'layouts/list'} );
        console.log("employees list renders successfully");
        });
};



exports.employee_detail = function(req, res, next) {
    
            models.Employee.findById(
                req.params.employee_id,
                
                { include: [
                    { model: models.Role,
                      attributes: ['id', 'roleName']
                    },
                    {
                      model: models.Department,
                      attributes: ['id', 'departmentName']
                    }
                    ]

                    
                }
                
        ).then(function(employee) {
        // renders an inividual employee details page
        res.render('pages/employee_detail', { title: 'Employee Details', employee: employee, layout: 'layouts/detail'} );
        console.log("employee detials renders successfully");
        });
    
};








// exports.index = function(req, res, next) {
//     models.Employee.findAndCountAll(
//       ).then(function(employeeCount) {
          
//     models.Department.findAndCountAll().then(function(departmentCount) {
        
//     models.Role.findAndCountAll().then(function(roleCount) {
        
//     models.Profile.findAndCountAll().then(function(profileCount) {
        
//     models.Post.findAndCountAll().then(function(postCount) {
        
//     models.Currentbusiness.findAndCountAll().then(function(currentBusinessCount) {
        
        
//     res.render('pages/index', {title: 'Homepage', employeeCount: employeeCount,  departmentCount:  departmentCount,
//     roleCount: roleCount, profileCount: profileCount, postCount: postCount, currentBusinessCount: currentBusinessCount, layout: 'layouts/main'});

//                         })
//                     })
//                 })
                 
//             })
        
//         })
    
//     })
// };
