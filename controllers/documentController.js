var Document = require('../models/document');
var models = require('../models');
var async = require('async');



exports.document_create_get = async function(req, res, next) {
        // renders a document form
        
        const types = await models.Type.findAll();
        const applications = await models.Application.findAll();
        const categories = await models.Category.findAll();
        const departments = await models.Department.findAll();
        const roles = await models.Role.findAll();
        const employees = await models.employee.findAll();
        res.render('forms/document_form', { title: 'Create Document',types: types, applications: applications,
        departments: departments, roles: roles, employees: employees, categories: categories, layout: 'layouts/detail'});
        console.log("document form renders successfully");
       
};

// Handle document create on post.
exports.document_create_post = async function(req, res, next) {
     
         const document = await models.Document.create({
             
             
                subject: req.body.subject,
                
                description: req.body.description_,
                
                status: req.body.status_,
                
                TypeId: req.body.type_id,
                
                EmployeeId: req.body.employee_id,
                
                ApplicationId: req.body.application_id,
                
                DepartmentId: req.body.department_id,
                
                RoleId: req.body.role_id,
                
                CategoryId: req.body.category_id
                });
            
                console.log("The saved document " + document.id);
                
                res.redirect('/main/documents');
        
     
     
};

exports.document_delete_get = function(req, res, next) {
    
         models.Document.destroy({
            // find the document_id to delete from database
            where: {
              id: req.params.document_id
            }
          });
    
            res.redirect('/main/documents');
            console.log("document deleted successfully");
};

// Handle document delete on post.
exports.document_delete_post = function(req, res, next) {
        
         models.Document.destroy({
            where: {
              id: req.params.document_id
            }
          });
    
          res.redirect('/main/documents');
          console.log("document deleted successfully");

 };

// Display document update form on GET.
exports.document_update_get = async function(req, res, next) {
    
        const document = await models.Document.findById(req.params.document_id);
               // renders a document form
               res.render('forms/document_form', { title: 'Update Document', document: document, layout: 'layouts/detail'});
               console.log("document update get successful");
        
};

// Handle document update on post.
exports.document_update_post = async function(req, res, next) {
    

            models.Document.update(
            {
                subject: req.body.subject,
                
                description: req.body.description_,
                
                status: req.body.status_,
                
                TypeId: req.body.type_id,
                
                EmployeeId: req.body.employee_id,
                
                ApplicationId: req.body.application_id,
                
                DepartmentId: req.body.department_id,
                
                RoleId: req.body.role_id,
                
                CategoryId: req.body.category_id
            },
          { // Clause
                where: 
                {
                    id: req.params.document_id
                }
            });
                res.redirect("/main/documents");  
                console.log("document updated successfully");
   
};

// Display detail page for a specific document.
exports.document_detail = async function(req, res, next) {
       
               const document = await models.Document.findById(
                req.params.document_id,
                       {
                    
                    include: [
                    {
                      model: models.Employee,
                      attributes: ['id', 'firstName', 'lastName']
                    },
                    ]
                    
                }
        )
        // renders an inividual document details page
        res.render('pages/document_detail', { title: 'Document Details', document: document, layout: 'layouts/detail'} );
        console.log("document deteials renders successfully");
    };


// Display list of all documents.
exports.document_list = async function(req, res, next) {
        // controller logic to display all documents
        const employee = await models.Employee.findAll();
             const documents = await models.Document.findAll({
                include: [
                    { 
                   
                    model: models.Employee,
                    attributes: ["id","firstName", "lastName"]
                   
                }]
            });
     
            console.log("rendering document list");
            res.render('pages/document_list', { title: 'Document List', layout: 'layouts/list', documents: documents, employee: employee} );
            console.log("documents list renders successfully")
        
};
