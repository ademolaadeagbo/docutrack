var Department = require('../models/department');
var models = require('../models');

 
// Display department create form on GET.
exports.department_create_get = function(req, res, next) {
        // create department GET controller logic here 
        res.render('forms/department_form', { title: 'Create Department', layout: 'layouts/detail'});
        console.log("department form renders successfully");
};

// Handle department create on POST.
exports.department_create_post = function(req, res, next) {
     // create department POST controller logic here
     // If a department gets created successfully, we just redirect to departments list
     // no need to render a page
      models.Department.create({
            departmentName: req.body.departmentName,
        }).then(function() {
            console.log("department created successfully");
           // check if there was an error during article creation
            res.redirect('/main/departments');
      });
};

// Display department delete form on GET.
exports.department_delete_get = function(req, res, next) {
        // GET logic to delete a department here
        
        // renders department delete page
        models.Department.destroy({
            // find the department_id to delete from database
            where: {
              id: req.params.department_id
            }
          }).then(function() {
           // If a department gets deleted successfully, we just redirect departments list
           // no need to render a page
            res.redirect('/main/departments');
            console.log("department deleted successfully");
          });
};

// Handle department delete on POST.
exports.department_delete_post = function(req, res, next) {
        // POST logic to delete a department here
        // If an department gets deleted successfully, we just redirect to departments list
        // no need to render a page
        models.Department.destroy({
            // find the department_id to delete from database
            where: {
              id: req.params.department_id
            }
          }).then(function() {
           // If an article gets deleted successfully, we just redirect to articles list
           // no need to render a page
            res.redirect('/main/departments');
            console.log("department deleted successfully");
          });
};

// Display department update form on GET.
exports.department_update_get = function(req, res, next) {
        // GET logic to update a department here
        
        console.log("ID is " + req.params.department_id);
        models.Department.findById(
                req.params.department_id
        ).then(function(department) {
               // renders a department form
               res.render('forms/department_form', { title: 'Update Department', department: department, layout: 'layouts/detail'});
               console.log("department update get successful");
          });
};

// Handle post update on POST.
exports.department_update_post = function(req, res, next) {
        // POST logic to update an department here
        // If an department gets updated successfully, we just redirect to departments list
        // no need to render a page
        models.Department.update(
        // Values to update
            {
                departmentName: req.body.departmentName
            },
          { // Clause
                where: 
                {
                    id: req.params.department_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function() { 
                // If a department updated successfully, we just redirect to departments list
                // no need to render a page
                res.redirect("/main/departments");  
                console.log("department updated successfully");
          });
};

// Display list of all departments.
exports.department_list = function(req, res, next) {
        // GET controller logic to list all departments
         models.Department.findAll(
        ).then(function(departments) {
       // renders all departments list
        console.log("rendering department list");
        res.render('pages/department_list', { title: 'Department List', departments: departments, layout: 'layouts/list'} );
        console.log("departments list renders successfully");
        });
};

// Display detail page for a specific department.
exports.department_detail = async function(req, res, next) {
        // GET controller logic to display just one department
        
        // renders an individual department details page
         const employees = await models.Post.findAll();
         models.Department.findById(
                req.params.department_id,  {
                include: [
                    {
                      model: models.Employee
                    }
                         ]
                }
        ).then(function(department) {
        // renders an inividual article details page
        res.render('pages/department_detail', { title: 'Department Details', department: department, employees: employees, layout: 'layouts/detail'} );
        console.log("department details renders successfully");
        });
};