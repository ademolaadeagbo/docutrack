var Role = require('../models/role');
var models = require('../models');

 
// Display role create form on GET.
exports.role_create_get = function(req, res, next) {
        // create role GET controller logic here 
        res.render('forms/role_form', { title: 'Create Role', layout: 'layouts/detail'});
        console.log("role form renders successfully");
};

// Handle role create on POST.
exports.role_create_post = function(req, res, next) {
     // create role POST controller logic here
     // If a role gets created successfully, we just redirect to roles list
     // no need to render a page
      models.Role.create({
            roleName: req.body.roleName,
        }).then(function() {
            console.log("role created successfully");
           // check if there was an error during article creation
            res.redirect('/main/roles');
      });
};

// Display role delete form on GET.
exports.role_delete_get = function(req, res, next) {
        // GET logic to delete a role here
        
        // renders role delete page
        models.Role.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.role_id
            }
          }).then(function() {
           // If a role gets deleted successfully, we just redirect roles list
           // no need to render a page
            res.redirect('/main/roles');
            console.log("role deleted successfully");
          });
};

// Handle role delete on POST.
exports.role_delete_post = function(req, res, next) {
        // POST logic to delete a role here
        // If an role gets deleted successfully, we just redirect to roles list
        // no need to render a page
        models.Role.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.role_id
            }
          }).then(function() {
           // If an article gets deleted successfully, we just redirect to articles list
           // no need to render a page
            res.redirect('/main/roles');
            console.log("role deleted successfully");
          });
};

// Display role update form on GET.
exports.role_update_get = function(req, res, next) {
        // GET logic to update a role here
        
        console.log("ID is " + req.params.role_id);
        models.Role.findById(
                req.params.role_id
        ).then(function(role) {
               // renders a role form
               res.render('forms/role_form', { title: 'Update Role', role: role, layout: 'layouts/detail'});
               console.log("role update get successful");
          });
};

// Handle post update on POST.
exports.role_update_post = function(req, res, next) {
        // POST logic to update an role here
        // If an role gets updated successfully, we just redirect to roles list
        // no need to render a page
        models.Role.update(
        // Values to update
            {
                roleName: req.body.roleName
            },
          { // Clause
                where: 
                {
                    id: req.params.role_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function() { 
                // If a role updated successfully, we just redirect to roles list
                // no need to render a page
                res.redirect("/main/roles");  
                console.log("role updated successfully");
          });
};

// Display list of all roles.
exports.role_list = function(req, res, next) {
        // GET controller logic to list all roles
         models.Role.findAll(
        ).then(function(roles) {
       // renders all roles list
        console.log("rendering role list");
        res.render('pages/role_list', { title: 'Role List', roles: roles, layout: 'layouts/list'} );
        console.log("roles list renders successfully");
        });
};

// Display detail page for a specific role.
exports.role_detail = async function(req, res, next) {
        // GET controller logic to display just one role
        
        // renders an individual role details page
         const employees = await models.Post.findAll();
         models.Role.findById(
                req.params.role_id,  {
                include: [
                    {
                      model: models.Employee
                    }
                         ]
                }
        ).then(function(role) {
        // renders an inividual article details page
        res.render('pages/role_detail', { title: 'Role Details', role: role, employees: employees, layout: 'layouts/detail'} );
        console.log("role details renders successfully");
        });
};