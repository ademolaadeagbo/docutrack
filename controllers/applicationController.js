var Application = require('../models/application');
var models = require('../models');

 
// Display application create form on GET.
exports.application_create_get = function(req, res, next) {
        // create application GET controller logic here 
        res.render('forms/application_form', { title: 'Create Application', layout: 'layouts/detail'});
        console.log("application form renders successfully");
};

// Handle application create on POST.
exports.application_create_post = function(req, res, next) {
     // create application POST controller logic here
     // If a application gets created successfully, we just redirect to applications list
     // no need to render a page
      models.Application.create({
            applicationName: req.body.application,
        }).then(function() {
            console.log("application created successfully");
           // check if there was an error during article creation
            res.redirect('/main/applications');
      });
};

// Display application delete form on GET.
exports.application_delete_get = function(req, res, next) {
        // GET logic to delete a application here
        
        // renders application delete page
        models.Application.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.application_id
            }
          }).then(function() {
           // If a application gets deleted successfully, we just redirect applications list
           // no need to render a page
            res.redirect('/main/applications');
            console.log("application deleted successfully");
          });
};

// Handle application delete on POST.
exports.application_delete_post = function(req, res, next) {
        // POST logic to delete a application here
        // If an application gets deleted successfully, we just redirect to applications list
        // no need to render a page
        models.Application.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.application_id
            }
          }).then(function() {
           // If an article gets deleted successfully, we just redirect to articles list
           // no need to render a page
            res.redirect('/main/applications');
            console.log("application deleted successfully");
          });
};

// Display application update form on GET.
exports.application_update_get = function(req, res, next) {
        // GET logic to update a application here
        
        console.log("ID is " + req.params.application_id);
        models.Application.findById(
                req.params.application_id
        ).then(function(application) {
               // renders a application form
               res.render('forms/application_form', { title: 'Update Application', application: application, layout: 'layouts/detail'});
               console.log("application update get successful");
          });
};

// Handle post update on POST.
exports.application_update_post = function(req, res, next) {
        // POST logic to update an application here
        // If an application gets updated successfully, we just redirect to applications list
        // no need to render a page
        models.Application.update(
        // Values to update
            {
                applicationName: req.body.application
            },
          { // Clause
                where: 
                {
                    id: req.params.application_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function() { 
                // If a application updated successfully, we just redirect to applications list
                // no need to render a page
                res.redirect("/main/applications");  
                console.log("application updated successfully");
          });
};

// Display list of all applications.
exports.application_list = function(req, res, next) {
        // GET controller logic to list all applications
         models.Application.findAll(
        ).then(function(applications) {
       // renders all applications list
        console.log("rendering application list");
        res.render('pages/application_list', { title: 'Application List', applications: applications, layout: 'layouts/list'} );
        console.log("applications list renders successfully");
        });
};

// Display detail page for a specific application.
exports.application_detail = async function(req, res, next) {
        // GET controller logic to display just one application
        
        // renders an individual application details page
         const documents = await models.Post.findAll();
         models.Application.findById(
                req.params.application_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(application) {
        // renders an inividual article details page
        res.render('pages/application_detail', { title: 'Application Details', application: application, documents: documents, layout: 'layouts/detail'} );
        console.log("application details renders successfully");
        });
};