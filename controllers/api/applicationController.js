var Application = require('../../models/application');
var models = require('../../models');

 

// Handle application create on POST.
exports.application_create_post = function(req, res, next) {
     // create application POST controller logic here
     // If a application gets created successfully, we just redirect to applications list
     // no need to render a page
      models.Application.create({
            applicationName: req.body.applicationName,
        }).then(function(application) {
            res.json({
            success: 'Application Created Successfully',
            application: application
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Handle application delete on POST.
exports.application_delete_post = function(req, res, next) {
        // POST logic to delete a application here
        // If an application gets deleted successfully, we just redirect to applications list
        // no need to render a page
        models.Application.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.application_id
            }
          }).then(function() {
                     res.json({
                     success: 'Application Deleted Successfully',
             });
        }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
    })
};

// Handle post update on POST.
exports.application_update_post = function(req, res, next) {
        // POST logic to update an application here
        // If an application gets updated successfully, we just redirect to applications list
        // no need to render a page
        models.Application.update(
        // Values to update
            {
                applicationName: req.body.applicationName
            },
          { // Clause
                where: 
                {
                    id: req.params.application_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function(application) {
            res.json({
            success: 'Application Updated Successfully',
            application: application
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
// Display list of all applications.
exports.application_list = function(req, res, next) {
        // GET controller logic to list all applications
         models.Application.findAll(
        ).then(function(applications) {
           res.json({
            success: 'All Applications',
            applications: applications
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Display detail page for a specific application.
exports.application_detail = async function(req, res, next) {
        // GET controller logic to display just one application
        
        // renders an individual application details page
         const documents = await models.Document.findAll();
         models.Application.findById(
                req.params.application_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(application) {
            res.json({
            success: 'Application Detail',
            application: application
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
