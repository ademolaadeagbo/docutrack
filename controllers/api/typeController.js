var Type = require('../../models/type');
var models = require('../../models');

 
// Handle type create on POST.
exports.type_create_post = function(req, res, next) {
     // create type POST controller logic here
     // If a type gets created successfully, we just redirect to types list
     // no need to render a page
      models.Type.create({
            typeName: req.body.typeName,
        }).then(function(type) {
            res.json({
            success: 'Type Created Successfully',
            type: type
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Handle type delete on POST.
exports.type_delete_post = function(req, res, next) {
        // POST logic to delete a type here
        // If an type gets deleted successfully, we just redirect to types list
        // no need to render a page
        models.Type.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.type_id
            }
          }).then(function() {
                     res.json({
                     success: 'Type Deleted Successfully',
             });
        }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
    })
};

// Handle post update on POST.
exports.type_update_post = function(req, res, next) {
        // POST logic to update an type here
        // If an type gets updated successfully, we just redirect to types list
        // no need to render a page
        models.Type.update(
        // Values to update
            {
                typeName: req.body.typeName
            },
          { // Clause
                where: 
                {
                    id: req.params.type_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function(type) {
            res.json({
            success: 'Type Updated Successfully',
            type: type
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
// Display list of all types.
exports.type_list = function(req, res, next) {
        // GET controller logic to list all types
         models.Type.findAll(
        ).then(function(types) {
           res.json({
            success: 'All Types',
            types: types
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Display detail page for a specific type.
exports.type_detail = async function(req, res, next) {
        // GET controller logic to display just one type
        
        // renders an individual type details page
         const documents = await models.Document.findAll();
         models.Type.findById(
                req.params.type_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(type) {
            res.json({
            success: 'Type Detail',
            type: type
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
