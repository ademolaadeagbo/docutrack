var Document = require('../../models/document');
var models = require('../../models');
var async = require('async');




// Handle document create on post.
exports.document_create_post = function(req, res, next) {
     
         models.Document.create({
             
                subject: req.body.subject,
                
                description: req.body.description,
                
                status: req.body.status,
                
                TypeId: req.body.type_id,
                
                EmployeeId: req.body.employee_id,
                
                ApplicationId: req.body.application_id,
                
                DepartmentId: req.body.department_id,
                
                RoleId: req.body.role_id,
                
                CategoryId: req.body.category_id
                
                }).then(function(document_) {
                    
                    res.json({
                    success: 'Document Created Successfully',
                    document_: document_
                });
            }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
     })
};


// Handle document delete on post.
exports.document_delete_post = function(req, res, next) {
        
         models.Document.destroy({
            where: {
              id: req.params.document_id
            }
          }).then(function() {
                     res.json({
                     success: 'Document Deleted Successfully',
                      });
            }).catch(error => {
                    console.log("There was an error: " + error);
                    res.status(404).send(error);
        })
};



// Handle document update on post.
exports.document_update_post = async function(req, res, next) {
    

            models.Document.update(
            {
                subject: req.body.subject,
                
                description: req.body.description,
                
                status: req.body.status,
                
                TypeId: req.body.type_id,
                
                EmployeeId: req.body.employee_id,
                
                ApplicationId: req.body.application_id,
                
                DepartmentId: req.body.department_id,
                
                RoleId: req.body.role_id,
                
                CategoryId: req.body.category_id
            },
          { // Clause
                where: 
                {
                    id: req.params.document_id
                }
            }).then(function(document_) {
                    
                    res.json({
                    success: 'Document Updated Successfully',
                    document_: document_
                });
            }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
     })
};


// Display detail page for a specific document.
exports.document_detail = async function(req, res, next) {
       
               const document = await models.Document.findById(
                req.params.document_id,
                       {
                    
                    include: [
                    {
                      model: models.Employee,
                      attributes: ['id', 'firstName', 'lastName']
                    },
                    ]
                    
                }
        ).then(function(document_) {
            res.json({
            success: 'Document Detail',
            document_: document_
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};



// Display list of all documents.
exports.document_list = async function(req, res, next) {
        // controller logic to display all documents
        const employee = await models.Employee.findAll();
        const documents = await models.Document.findAll({
                include: [
                    { 
                   
                    model: models.Employee,
                    attributes: ["id","firstName", "lastName"]
                   
                }]
            }).then(function(documents) {
                res.json({
                success: 'All Documents',
                documents: documents
            });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
