var Role = require('../../models/role');
var models = require('../../models');

 
// Handle role create on POST.
exports.role_create_post = function(req, res, next) {
     // create role POST controller logic here
     // If a role gets created successfully, we just redirect to roles list
     // no need to render a page
      models.Role.create({
            roleName: req.body.roleName,
        }).then(function(role) {
            res.json({
            success: 'Role Created Successfully',
            role: role
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Handle role delete on POST.
exports.role_delete_post = function(req, res, next) {
        // POST logic to delete a role here
        // If an role gets deleted successfully, we just redirect to roles list
        // no need to render a page
        models.Role.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.role_id
            }
          }).then(function() {
                     res.json({
                     success: 'Role Deleted Successfully',
             });
        }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
    })
};

// Handle post update on POST.
exports.role_update_post = function(req, res, next) {
        // POST logic to update an role here
        // If an role gets updated successfully, we just redirect to roles list
        // no need to render a page
        models.Role.update(
        // Values to update
            {
                roleName: req.body.roleName
            },
          { // Clause
                where: 
                {
                    id: req.params.role_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function(role) {
            res.json({
            success: 'Role Updated Successfully',
            role: role
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
// Display list of all roles.
exports.role_list = function(req, res, next) {
        // GET controller logic to list all roles
         models.Role.findAll(
        ).then(function(roles) {
           res.json({
            success: 'All Roles',
            roles: roles
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Display detail page for a specific role.
exports.role_detail = async function(req, res, next) {
        // GET controller logic to display just one role
        
        // renders an individual role details page
         const documents = await models.Document.findAll();
         models.Role.findById(
                req.params.role_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(role) {
            res.json({
            success: 'Role Detail',
            role: role
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
