var Comment = require('../../models/comment');
var models = require('../../models');

 
// Handle comment create on POST.
exports.comment_create_post = function(req, res, next) {
     // create comment POST controller logic here
     // If a comment gets created successfully, we just redirect to comments list
     // no need to render a page
      models.Comment.create({
            commentBody: req.body.commentBody,
        }).then(function(comment) {
            res.json({
            success: 'Comment Created Successfully',
            comment: comment
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Handle comment delete on POST.
exports.comment_delete_post = function(req, res, next) {
        // POST logic to delete a comment here
        // If an comment gets deleted successfully, we just redirect to comments list
        // no need to render a page
        models.Comment.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.comment_id
            }
          }).then(function() {
                     res.json({
                     success: 'Comment Deleted Successfully',
             });
        }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
    })
};

// Handle post update on POST.
exports.comment_update_post = function(req, res, next) {
        // POST logic to update an comment here
        // If an comment gets updated successfully, we just redirect to comments list
        // no need to render a page
        models.Comment.update(
        // Values to update
            {
                commentBody: req.body.commentBody
            },
          { // Clause
                where: 
                {
                    id: req.params.comment_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function(comment) {
            res.json({
            success: 'Comment Updated Successfully',
            comment: comment
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
// Display list of all comments.
exports.comment_list = function(req, res, next) {
        // GET controller logic to list all comments
         models.Comment.findAll(
        ).then(function(comments) {
           res.json({
            success: 'All Comments',
            comments: comments
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Display detail page for a specific comment.
exports.comment_detail = async function(req, res, next) {
        // GET controller logic to display just one comment
        
        // renders an individual comment details page
         const documents = await models.Document.findAll();
         models.Comment.findById(
                req.params.comment_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(comment) {
            res.json({
            success: 'Comment Detail',
            comment: comment
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
