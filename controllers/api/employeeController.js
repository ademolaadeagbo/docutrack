var Employee = require('../../models/employee');
var models = require('../../models');
var async = require('async');




// Handle employee create on POST.
exports.employee_create_post = function(req, res, next) {
     // create employee POST controller logic here
     // If an employee gets created successfully, we just redirect to employees list
     // no need to render a page
    models.Employee.create({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            userName: req.body.userName,
            email: req.body.email,
            phone: req.body.phone,
            address: req.body.address,
            DepartmentId: req.body.department,
            RoleId: req.body.role,
            
        }).then(function(employee) {
                    res.json({
                    success: 'Employee Created Successfully',
                    employee: employee
                });
            }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
     })
};

// Handle employee delete on POST.
exports.employee_delete_post = function(req, res, next) {
        // POST logic to delete a employee here
        

         models.Employee.destroy({
            // find the employee_id to delete from database
            where: {
              id: req.params.employee_id
            }
          }).then(function() {
                    
                    res.json({
                    success: 'Employee Deleted Successfully',
                });
            }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
     })
};



// Handle employee update on POST.
exports.employee_update_post =  function(req, res, next) {
        // POST logic to update a employee here

        



         models.Employee.update(
        // Values to update
            {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                userName: req.body.userName,
                email: req.body.email,
                phone: req.body.phone,
                address: req.body.address,
                DepartmentId: req.body.department,
                RoleId: req.body.role,
            },
            { // Clause
                where: 
                {
                    id: req.params.employee_id
                }
            }
       
         ).then(function(employee) {
                    
                    res.json({
                    success: 'Employee Updated Successfully',
                    employee: employee
                });
            }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
     })
};






exports.employee_list = function(req, res, next) {
        // GET controller logic to list all employees
        
        
        
        models.Employee.findAll({
         include: [{
                        
                  model: models.Role,
                  attributes: ['id', 'roleName']
             
                },
                {
                        
                  model: models.Department,
                  attributes: ['id', 'departmentName']
             
                }]
        
        }).then(function(employees) {
                res.json({
                success: 'All Employees',
                employees: employees
            });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};


exports.employee_detail = function(req, res, next) {
    
            models.Employee.findById(
                req.params.employee_id,
                
                { include: [
                    { model: models.Role,
                      attributes: ['id', 'roleName']
                    },
                    {
                      model: models.Department,
                      attributes: ['id', 'departmentName']
                    },
                    ]

                    
                }
                
        ).then(function(employee) {
                res.json({
                success: 'Employee Detail',
                employee: employee
            });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};








// exports.index = function(req, res, next) {
//     models.Employee.findAndCountAll(
//       ).then(function(employeeCount) {
          
//     models.Department.findAndCountAll().then(function(departmentCount) {
        
//     models.Role.findAndCountAll().then(function(roleCount) {
        
//     models.Profile.findAndCountAll().then(function(profileCount) {
        
//     models.Post.findAndCountAll().then(function(postCount) {
        
//     models.Currentbusiness.findAndCountAll().then(function(currentBusinessCount) {
        
        
//     res.render('pages/index', {title: 'Homepage', employeeCount: employeeCount,  departmentCount:  departmentCount,
//     roleCount: roleCount, profileCount: profileCount, postCount: postCount, currentBusinessCount: currentBusinessCount, layout: 'layouts/main'});

//                         })
//                     })
//                 })
                 
//             })
        
//         })
    
//     })
// };
