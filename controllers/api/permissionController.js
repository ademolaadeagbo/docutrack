var Permission = require('../../models/permission');
var models = require('../../models');


// Handle permission create on POST.
exports.permission_create_post = function(req, res, next) {
     // create permission POST controller logic here
     
      models.Permission.create({
            permissionName: req.body.permissionName
        }).then(function(permission) {
            res.json({
            success: 'Permission Created Successfully',
            permission: permission
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};


// Handle permission delete on POST.
exports.permission_delete_post = function(req, res, next) {
        // POST logic to delete a permission here
        models.Permission.destroy({
            where: {
              id: req.params.permission_id
            }
          }).then(function() {
            res.json({
            success: 'Permission Deleted Successfully',
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};


// Handle permission update on POST.
exports.permission_update_post = function(req, res, next) {
        // POST logic to update a permission here
       
         models.Permission.update(
        // Values to update
            {
                permissionName: req.body.permissionName,
            },
            
            { // Clause
                where: 
                {
                    id: req.params.permission_id
                }
            }
      
         ).then(function(permission) {
            res.json({
            success: 'Permission Updated Successfully',
            permission: permission
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Display list of all permissions.
exports.permission_list = function(req, res, next) {
        // controller logic to display all permissions
         models.Permission.findAll(
             
             
             {
                    include: [
                     {
                          model: models.Employee,
                          as: 'employees',
                          required: false,
                          // Pass in the Permission attributes that you want to retrieve
                          attributes: ['id', 'firstName', 'lastName'],
                          through: {
                            // This block of code allows you to retrieve the properties of the join table EmployeePermissions
                            model: models.EmployeePermissions,
                            as: 'employeePermissions',
                            attributes: ['employeeId', 'permissionId'],
                        }
                    }
                ]
              }
             
             
             
        ).then(function(permissions) {
            res.json({
            success: 'All Permissions',
            permissions: permissions
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
// Display detail page for a specific permission.
exports.permission_detail = function(req, res, next) {
        // constroller logic to display a single permission
         models.Permission.findById(
              req.params.permission_id,
              {
                    include: [
                     {
                          model: models.Employee,
                          as: 'employees',
                          required: false,
                          // Pass in the Permission attributes that you want to retrieve
                         // attributes: ['id', 'firstName', 'lastName'],
                          through: {
                            // This block of code allows you to retrieve the properties of the join table PostPermissions
                            model: models.EmployeePermissions,
                            as: 'employeePermissions',
                            attributes: ['employeeId', 'permissionId'],
                        }
                    }
                ]
              }
        ).then(function(permission) {
            res.json({
            success: 'Permission Detail',
            permission: permission
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

 