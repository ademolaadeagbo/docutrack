var Category = require('../../models/category');
var models = require('../../models');

 

// Handle category create on POST.
exports.category_create_post = function(req, res, next) {
     // create category POST controller logic here
     // If a category gets created successfully, we just redirect to categorys list
     // no need to render a page
      models.Category.create({
            categoryName: req.body.categoryName,
        }).then(function(category) {
            res.json({
            success: 'Category Created Successfully',
            category: category
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Handle category delete on POST.
exports.category_delete_post = function(req, res, next) {
        // POST logic to delete a category here
        // If an category gets deleted successfully, we just redirect to categorys list
        // no need to render a page
        models.Category.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.category_id
            }
          }).then(function() {
                     res.json({
                     success: 'Category Deleted Successfully',
             });
        }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
    })
};

// Handle post update on POST.
exports.category_update_post = function(req, res, next) {
        // POST logic to update an category here
        // If an category gets updated successfully, we just redirect to categorys list
        // no need to render a page
        models.Category.update(
        // Values to update
            {
                categoryName: req.body.categoryName
            },
          { // Clause
                where: 
                {
                    id: req.params.category_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function(category) {
            res.json({
            success: 'Category Updated Successfully',
            category: category
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
// Display list of all categorys.
exports.category_list = function(req, res, next) {
        // GET controller logic to list all categorys
         models.Category.findAll(
        ).then(function(categories) {
           res.json({
            success: 'All Categories',
            categories: categories
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
// Display detail page for a specific category.
exports.category_detail = async function(req, res, next) {
        // GET controller logic to display just one category
        
        // renders an individual category details page
         const documents = await models.Document.findAll();
         models.Category.findById(
                req.params.category_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(category) {
            res.json({
            success: 'Category Detail',
            category: category
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
