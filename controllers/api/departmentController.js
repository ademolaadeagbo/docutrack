var Department = require('../../models/department');
var models = require('../../models');

 
// Handle department create on POST.
exports.department_create_post = function(req, res, next) {
     // create department POST controller logic here
     // If a department gets created successfully, we just redirect to departments list
     // no need to render a page
      models.Department.create({
            departmentName: req.body.departmentName,
        }).then(function(department) {
            res.json({
            success: 'Department Created Successfully',
            department: department
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Handle department delete on POST.
exports.department_delete_post = function(req, res, next) {
        // POST logic to delete a department here
        // If an department gets deleted successfully, we just redirect to departments list
        // no need to render a page
        models.Department.destroy({
            // find the article_id to delete from database
            where: {
              id: req.params.department_id
            }
          }).then(function() {
                     res.json({
                     success: 'Department Deleted Successfully',
             });
        }).catch(error => {
                console.log("There was an error: " + error);
                res.status(404).send(error);
    })
};

// Handle post update on POST.
exports.department_update_post = function(req, res, next) {
        // POST logic to update an department here
        // If an department gets updated successfully, we just redirect to departments list
        // no need to render a page
        models.Department.update(
        // Values to update
            {
                departmentName: req.body.departmentName
            },
          { // Clause
                where: 
                {
                    id: req.params.department_id
                }
            }
        //   returning: true, where: {id: req.params.article_id} 
         ).then(function(department) {
            res.json({
            success: 'Department Updated Successfully',
            department: department
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
// Display list of all departments.
exports.department_list = function(req, res, next) {
        // GET controller logic to list all departments
         models.Department.findAll(
        ).then(function(departments) {
           res.json({
            success: 'All Departments',
            departments: departments
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};

// Display detail page for a specific department.
exports.department_detail = async function(req, res, next) {
        // GET controller logic to display just one department
        
        // renders an individual department details page
         const documents = await models.Document.findAll();
         models.Department.findById(
                req.params.department_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(department) {
            res.json({
            success: 'Department Detail',
            department: department
        });
    }).catch(error => {
        console.log("There was an error: " + error);
        res.status(404).send(error);
    })
};
