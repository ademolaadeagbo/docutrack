var Category = require('../models/category');
var models = require('../models');

// Display category create form on GET.
exports.category_create_get = function(req, res, next) {
        // create category GET controller logic here 

        // renders a category form
        res.render('forms/category_form', { title: 'Create Category',  layout: 'layouts/detail'});
};

// Handle category create on POST.
exports.category_create_post = function(req, res, next) {
     // create category POST controller logic here
     
      models.Category.create({
            categoryName: req.body.name
        }).then(function() {
            console.log('category created successfully')
           // check if there was an error during category creation
     res.redirect("/main/categories")
                
        });
};

// Display category delete form on GET.
exports.category_delete_get = function(req, res, next) {
        // GET logic to delete a category here
        models.Category.destroy({
            // find the category_id to delete from database
            where: {
              id: req.params.category_id
            }
          }).then(function() {
          // renders delete page
        res.redirect('/main/categories');
        console.log("category deleted successfully");
          });
};

// Handle category delete on POST.
exports.category_delete_post = function(req, res, next) {
        // POST logic to delete a category here
        models.Category.destroy({
            where: {
              id: req.params.category_id
            }
          }).then(function() {
           // If an category gets deleted successfully, we just redirect to categories list
           // no need to render a page
            res.redirect('/main/categories');
            console.log("category deleted successfully");
          });
};

// Display category update form on GET.
exports.category_update_get = function(req, res, next) {
        // GET logic to update a category here
        console.log("ID is " + req.params.category_id);
        models.Category.findById(
                req.params.category_id
        ).then(function(category) {
                
        res.render('forms/category_form', { title: 'Update Category', category: category,  layout: 'layouts/detail' });
        console.log("category update get successful");
      });
};

// Handle category update on POST.
exports.category_update_post = function(req, res, next) {
        // POST logic to update a category here
       
         models.Category.update(
        // Values to update
            {
                categoryName: req.body.name,
            },
            
            { // Clause
                where: 
                {
                    id: req.params.category_id
                }
            }
      
         ).then(function() { 
                
          
        res.redirect("/main/categories");
        console.log("category updated successfully");
    });
};

// Display list of all categories.
exports.category_list = function(req, res, next) {
        // controller logic to display all categories
         models.Category.findAll(
             
             
             {
                    include: [
                     {
                          model: models.Document,
                          as: 'documents',
                          required: false,
                          // Pass in the Category attributes that you want to retrieve
                          attributes: ['id', 'category_title', 'category_body'],
                          through: {
                            // This block of code allows you to retrieve the properties of the join table PostCategories
                            model: models.DocumentCategories,
                            as: 'documentCategories',
                            attributes: ['category_id', 'category_id'],
                        }
                    }
                ]
              }
             
             
             
        ).then(function(categories) {
      
         console.log("rendering category list");
        res.render('pages/category_list', { title: 'Category List', categories: categories,  layout: 'layouts/list'} );
        console.log("category list renders successfully");
   });
};

// Display detail page for a specific category.
exports.category_detail = function(req, res, next) {
        // constroller logic to display a single category
         models.Category.findById(
              req.params.category_id,
              {
                    include: [
                     {
                          model: models.Document,
                          as: 'documents',
                          required: false,
                          // Pass in the Category attributes that you want to retrieve
                          attributes: ['id', 'subject', 'description'],
                          through: {
                            // This block of code allows you to retrieve the properties of the join table PostCategories
                            model: models.DocumentCategories,
                            as: 'categoryCategories',
                            attributes: ['documentId', 'categoryId'],
                        }
                    }
                ]
              }
        ).then(function(category) {
        
        res.render('pages/category_detail', { title: 'Category Details', category: category, layout: 'layouts/detail'} );
        console.log("Caegory details renders successfully");
      });
};

 