var Type = require('../models/type');
var models = require('../models');

 
// Display type create form on GET.
exports.type_create_get = function(req, res, next) {
        // create type GET controller logic here 
        res.render('forms/type_form', { title: 'Create Type', layout: 'layouts/detail'});
        console.log("type form renders successfully");
};

// Handle type create on POST.
exports.type_create_post = function(req, res, next) {
     // create type POST controller logic here
     // If a type gets created successfully, we just redirect to types list
     // no need to render a page
      models.Type.create({
            typeName: req.body.typeName,
        }).then(function() {
            console.log("type created successfully");
           // check if there was an error during type creation
            res.redirect('/main/types');
      });
};

// Display type delete form on GET.
exports.type_delete_get = function(req, res, next) {
        // GET logic to delete a type here
        
        // renders type delete page
        models.Type.destroy({
            // find the type_id to delete from database
            where: {
              id: req.params.type_id
            }
          }).then(function() {
           // If a type gets deleted successfully, we just redirect types list
           // no need to render a page
            res.redirect('/main/types');
            console.log("type deleted successfully");
          });
};

// Handle type delete on POST.
exports.type_delete_post = function(req, res, next) {
        // POST logic to delete a type here
        // If an type gets deleted successfully, we just redirect to types list
        // no need to render a page
        models.Type.destroy({
            // find the type_id to delete from database
            where: {
              id: req.params.type_id
            }
          }).then(function() {
           // If an type gets deleted successfully, we just redirect to types list
           // no need to render a page
            res.redirect('/main/types');
            console.log("type deleted successfully");
          });
};

// Display type update form on GET.
exports.type_update_get = function(req, res, next) {
        // GET logic to update a type here
        
        console.log("ID is " + req.params.type_id);
        models.Type.findById(
                req.params.type_id
        ).then(function(type) {
               // renders a type form
               res.render('forms/type_form', { title: 'Update Type', type: type, layout: 'layouts/detail'});
               console.log("type update get successful");
          });
};

// Handle post update on POST.
exports.type_update_post = function(req, res, next) {
        // POST logic to update an type here
        // If an type gets updated successfully, we just redirect to types list
        // no need to render a page
        models.Type.update(
        // Values to update
            {
                typeName: req.body.typeName
            },
          { // Clause
                where: 
                {
                    id: req.params.type_id
                }
            }
        //   returning: true, where: {id: req.params.type_id} 
         ).then(function() { 
                // If a type updated successfully, we just redirect to types list
                // no need to render a page
                res.redirect("/main/types");  
                console.log("type updated successfully");
          });
};

// Display list of all types.
exports.type_list = function(req, res, next) {
        // GET controller logic to list all types
         models.Type.findAll(
        ).then(function(types) {
       // renders all types list
        console.log("rendering type list");
        res.render('pages/type_list', { title: 'Type List', types: types, layout: 'layouts/list'} );
        console.log("types list renders successfully");
        });
};

// Display detail page for a specific type.
exports.type_detail = async function(req, res, next) {
        // GET controller logic to display just one type
        
        // renders an individual type details page
         const documents = await models.Document.findAll();
         models.Type.findById(
                req.params.type_id,  {
                include: [
                    {
                      model: models.Document
                    }
                         ]
                }
        ).then(function(type) {
        // renders an inividual type details page
        res.render('pages/type_detail', { title: 'Type Details', type: type, documents: documents, layout: 'layouts/detail'} );
        console.log("type details renders successfully");
        });
};