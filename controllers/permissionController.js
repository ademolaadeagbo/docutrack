var Permission = require('../models/permission');
var models = require('../models');

// Display permission create form on GET.
exports.permission_create_get = function(req, res, next) {
        // create permission GET controller logic here 

        // renders a permission form
        res.render('forms/permission_form', { title: 'Create Permission',  layout: 'layouts/detail'});
};

// Handle permission create on POST.
exports.permission_create_post = function(req, res, next) {
     // create permission POST controller logic here
     
      models.Permission.create({
            permissionName: req.body.name
        }).then(function() {
            console.log('permission created successfully')
           // check if there was an error during permission creation
     res.redirect("/main/permissions")
                
        });
};

// Display permission delete form on GET.
exports.permission_delete_get = function(req, res, next) {
        // GET logic to delete a permission here
        models.Permission.destroy({
            // find the employee_id to delete from database
            where: {
              id: req.params.permission_id
            }
          }).then(function() {
          // renders delete page
        res.redirect('/main/permissions');
        console.log("permission deleted successfully");
          });
};

// Handle permission delete on POST.
exports.permission_delete_post = function(req, res, next) {
        // POST logic to delete a permission here
        models.Permission.destroy({
            where: {
              id: req.params.permission_id
            }
          }).then(function() {
           // If an employee gets deleted successfully, we just redirect to employees list
           // no need to render a page
            res.redirect('/main/permissions');
            console.log("permission deleted successfully");
          });
};

// Display permission update form on GET.
exports.permission_update_get = function(req, res, next) {
        // GET logic to update a permission here
        console.log("ID is " + req.params.permission_id);
        models.Permission.findById(
                req.params.permission_id
        ).then(function(permission) {
                
        res.render('forms/permission_form', { title: 'Update Permission', permission: permission,  layout: 'layouts/detail' });
        console.log("permission update get successful");
      });
};

// Handle permission update on POST.
exports.permission_update_post = function(req, res, next) {
        // POST logic to update a permission here
       
         models.Permission.update(
        // Values to update
            {
                permissionName: req.body.name,
            },
            
            { // Clause
                where: 
                {
                    id: req.params.permission_id
                }
            }
      
         ).then(function() { 
                
          
        res.redirect("/main/permissions");
        console.log("permission updated successfully");
    });
};

// Display list of all permissions.
exports.permission_list = function(req, res, next) {
        // controller logic to display all permissions
         models.Permission.findAll(
             
             
             {
                    include: [
                     {
                          model: models.Employee,
                          as: 'employees',
                          required: false,
                          // Pass in the Permission attributes that you want to retrieve
                          attributes: ['id', 'employee_title', 'employee_body'],
                          through: {
                            // This block of code allows you to retrieve the properties of the join table EmployeePermissions
                            model: models.EmployeePermissions,
                            as: 'employeePermissions',
                            attributes: ['employeeId', 'permissionId'],
                        }
                    }
                ]
              }
             
             
             
        ).then(function(permissions) {
      
         console.log("rendering permission list");
        res.render('pages/permission_list', { title: 'Permission List', permissions: permissions,  layout: 'layouts/list'} );
        console.log("permission list renders successfully");
   });
};

// Display detail page for a specific permission.
exports.permission_detail = function(req, res, next) {
        // constroller logic to display a single permission
         models.Permission.findById(
              req.params.permission_id,
              {
                    include: [
                     {
                          model: models.Employee,
                          as: 'employees',
                          required: false,
                          // Pass in the Permission attributes that you want to retrieve
                         // attributes: ['id', 'firstName', 'lastName'],
                          through: {
                            // This block of code allows you to retrieve the properties of the join table PostPermissions
                            model: models.EmployeePermissions,
                            as: 'employeePermissions',
                            attributes: ['employeeId', 'permissionId'],
                        }
                    }
                ]
              }
        ).then(function(permission) {
        
        res.render('pages/permission_detail', { title: 'Permission Details', permission: permission, layout: 'layouts/detail'} );
        console.log("Caegory details renders successfully");
      });
};

 